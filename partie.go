package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
	"image/color"
)

type Partie struct {
	ball             *Ball
	player1, player2 Player
	uW, dW, rW, lW   *Wall
	points           float64
	terminated       bool
}

func NewPartie() *Partie {
	p := &Partie{}
	p.ball = NewBall(p)

	p.player1 = NewUpDownPlayer(p, ScreenWidth-WallWidth)
	//p.player1 = NewEvilPlayer(p, ScreenWidth-2)
	p.player2 = NewEvilPlayer(p, WallWidth)
	p.uW = NewWall(ScreenWidth, WallHeight, 0, 0, color.Transparent)
	p.dW = NewWall(ScreenWidth, WallHeight, 0, ScreenHeight-WallHeight, color.Transparent)
	p.rW = NewWall(WallWidth, ScreenHeight, ScreenWidth-WallWidth, 0, color.Transparent)
	p.lW = NewWall(WallWidth, ScreenHeight, 0, 0, color.Transparent)
	return p
}

func (p *Partie) Update() error {
	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		return ebiten.Termination
	}
	if !p.terminated {
		err := p.ball.Update()
		if err != nil {
			p.terminated = true
			return nil
		}
		p.player1.Update()
		p.player2.Update()
		p.points += p.ball.speed / 100
	}
	return nil
}

func (p *Partie) Draw(screen *ebiten.Image) {
	p.uW.Draw(screen)
	p.dW.Draw(screen)
	p.player1.Draw(screen)
	p.player2.Draw(screen)
	if !p.terminated {
		text.Draw(screen, fmt.Sprintf("%04d", int(p.points)), ScoreFont, int(ScreenWidth/2-100), 80, color.Black)
		p.rW.Draw(screen)
		p.lW.Draw(screen)
		p.ball.Draw(screen)
	} else {
		text.Draw(screen, "Game Over", ScoreFont, int(ScreenWidth/2-150), int(ScreenHeight/10), color.RGBA{R: 0xFF, A: 0xFF})
		text.Draw(screen, fmt.Sprintf("%.2f", p.points), ScoreFont, int(ScreenWidth-(ScreenWidth/4)), int(ScreenHeight-(ScreenHeight/12)), color.RGBA{R: 0xFF, A: 0xFF})
		text.Draw(screen, "Made by @ggerbaud", CreditsFont, int(ScreenWidth/2-150), int(ScreenHeight-(ScreenHeight/24)), color.RGBA{B: 0xFF, A: 0xFF})
	}
}
