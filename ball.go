package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"math"
	"math/rand"
)

type Ball struct {
	g                *Partie
	pos              Point
	speed, direction float64
	sprite           *ebiten.Image
}

func NewBall(g *Partie) *Ball {
	bounds := BallSprite.Bounds()
	dx, dy := float64(bounds.Dx())/2, float64(bounds.Dy())/2
	return &Ball{
		g:         g,
		pos:       Point{X: ScreenWidth/2 - dx, Y: ScreenHeight/2 - dy},
		speed:     5,
		direction: normalizeDirection(rand.Float64()*math.Pi/2 - math.Pi/4),
		//direction: math.Pi / 2,
		//direction: math.Pi - math.Pi/4,
		//direction: math.Pi,
		//direction: 0,
		sprite: BallSprite,
	}
}

func (b *Ball) Update() error {
	b.pos.X += b.speed * math.Cos(b.direction)
	b.pos.Y += b.speed * math.Sin(b.direction)
	bounds := b.sprite.Bounds()
	ballCollider := Collider{b.pos.X, b.pos.Y, float64(bounds.Dx()), float64(bounds.Dy())}
	playerLeftCollider := b.g.player1.Collider()
	playerRightCollider := b.g.player2.Collider()
	if playerLeftCollider.Intersects(ballCollider) {
		ratio := playerLeftCollider.Ratio(ballCollider)
		b.direction += ratio * math.Pi / 8
		b.direction = math.Pi - b.direction
		b.speed = b.speed * speedCoeff
	} else if playerRightCollider.Intersects(ballCollider) {
		ratio := playerRightCollider.Ratio(ballCollider)
		b.direction += ratio * math.Pi / 8
		b.direction = math.Pi - b.direction
		b.speed = b.speed * speedCoeff
	} else if b.g.uW.Collider().Intersects(ballCollider) || b.g.dW.Collider().Intersects(ballCollider) {
		b.direction = -b.direction
	} else if b.g.rW.Collider().Intersects(ballCollider) || b.g.lW.Collider().Intersects(ballCollider) {
		return fmt.Errorf("game lost")
	}
	b.direction = normalizeDirection(b.direction)
	return nil
}

func (b *Ball) Draw(screen *ebiten.Image) {
	//ebitenutil.DebugPrintAt(screen, fmt.Sprintf("Ball speed is %f, direction is %f.3", b.speed, b.direction), 200, 200)
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(b.pos.X, b.pos.Y)
	screen.DrawImage(b.sprite, op)
}

func normalizeDirection(dir float64) float64 {
	for dir > math.Pi {
		dir -= 2 * math.Pi
	}
	for dir < -math.Pi {
		dir += 2 * math.Pi
	}
	return dir
}
