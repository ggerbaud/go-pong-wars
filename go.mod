module go-pong-wars

go 1.21

require (
	github.com/hajimehoshi/ebiten/v2 v2.7.4
	golang.org/x/image v0.16.0
)

require (
	github.com/ebitengine/gomobile v0.0.0-20240518074828-e86332849895 // indirect
	github.com/ebitengine/hideconsole v1.0.0 // indirect
	github.com/ebitengine/oto/v3 v3.2.0 // indirect
	github.com/ebitengine/purego v0.7.0 // indirect
	github.com/go-text/typesetting v0.1.1-0.20240325125605-c7936fe59984 // indirect
	github.com/hajimehoshi/bitmapfont/v3 v3.0.0 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.4 // indirect
	github.com/jakecoffman/cp v1.2.1 // indirect
	github.com/jezek/xgb v1.1.1 // indirect
	github.com/jfreymuth/oggvorbis v1.0.5 // indirect
	github.com/jfreymuth/vorbis v1.0.2 // indirect
	github.com/kisielk/errcheck v1.7.0 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	golang.org/x/tools v0.21.0 // indirect
)
