package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"image/color"
	"math"
)

type EvilPlayer struct {
	g             *Partie
	pos           Point
	width, height float64
	sprite        *ebiten.Image
}

func NewEvilPlayer(g *Partie, x float64) Player {
	sprite := ebiten.NewImage(PlayerWidth, PlayerHeight)
	sprite.Fill(color.RGBA{R: 0xFF, A: 0xFF})

	bounds := sprite.Bounds()
	dx, dy := float64(bounds.Dx()), float64(bounds.Dy())
	halfW := dx / 2
	halfH := dy / 2

	pos := Point{
		X: x - halfW,
		Y: ScreenHeight/2 - halfH,
	}

	return &EvilPlayer{
		g:      g,
		pos:    pos,
		sprite: sprite,
		width:  dx,
		height: dy,
	}
}

func (p *EvilPlayer) Update() {
	//p.naturalUpdate()
	p.cheatedUpdate()
}

func (p *EvilPlayer) naturalUpdate() {
	ball := p.g.ball
	dir := ball.direction
	target := ScreenHeight / 2
	if dir > (math.Pi/2) || dir < -(math.Pi/2) {
		target = ball.pos.Y
	}
	target -= p.height / 2
	if target < 0 {
		target = 0
	}
	if target > ScreenHeight-p.height {
		target = ScreenHeight - p.height
	}
	if target > p.pos.Y {
		p.pos.Y += step
	}
	if target < p.pos.Y {
		p.pos.Y -= step
	}
}

func (p *EvilPlayer) cheatedUpdate() {
	ball := p.g.ball
	target := ball.pos.Y
	target -= p.height / 2
	if target < 0 {
		target = 0
	}
	if target > ScreenHeight-p.height {
		target = ScreenHeight - p.height
	}
	p.pos.Y = target
}

func (p *EvilPlayer) Draw(screen *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(p.pos.X, p.pos.Y)
	screen.DrawImage(p.sprite, op)
}

func (p *EvilPlayer) Collider() Collider {
	return Collider{p.pos.X, p.pos.Y, p.width, p.height}
}
