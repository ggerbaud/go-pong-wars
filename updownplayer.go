package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"image/color"
)

type UpDownPlayer struct {
	g             *Partie
	pos           Point
	width, height float64
	sprite        *ebiten.Image
}

func NewUpDownPlayer(g *Partie, x float64) Player {
	sprite := ebiten.NewImage(PlayerWidth, PlayerHeight)
	sprite.Fill(color.RGBA{G: 0xFF, A: 0xFF})

	bounds := sprite.Bounds()
	dx, dy := float64(bounds.Dx()), float64(bounds.Dy())
	halfW := dx / 2
	halfH := dy / 2

	pos := Point{
		X: x - halfW,
		Y: ScreenHeight/2 - halfH,
	}

	return &UpDownPlayer{
		g:      g,
		pos:    pos,
		sprite: sprite,
		width:  dx,
		height: dy,
	}
}

func (p *UpDownPlayer) Update() {
	if !p.g.terminated {
		if ebiten.IsKeyPressed(ebiten.KeyDown) {
			p.goDown()
		}
		if ebiten.IsKeyPressed(ebiten.KeyUp) {
			p.goUp()
		}
		if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
			_, y := ebiten.CursorPosition()
			midY := float64(y) - p.height/2
			if midY >= p.pos.Y {
				p.goDown()
			} else {
				p.goUp()
			}
		}
		touches := ebiten.AppendTouchIDs(nil)
		if len(touches) == 1 {
			_, y := ebiten.TouchPosition(touches[0])
			midY := float64(y) - p.height/2
			if midY >= p.pos.Y {
				p.goDown()
			} else {
				p.goUp()
			}
		}
	}
}

func (p *UpDownPlayer) goDown() {
	p.pos.Y += step
	if p.pos.Y > ScreenHeight-p.height {
		p.pos.Y = ScreenHeight - p.height
	}
}

func (p *UpDownPlayer) goUp() {
	p.pos.Y -= step
	if p.pos.Y < 0 {
		p.pos.Y = 0
	}
}

func (p *UpDownPlayer) Draw(screen *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(p.pos.X, p.pos.Y)
	screen.DrawImage(p.sprite, op)
}

func (p *UpDownPlayer) Collider() Collider {
	return Collider{p.pos.X, p.pos.Y, p.width, p.height}
}

const (
	step = 5
)
