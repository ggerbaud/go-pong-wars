package main

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Background struct {
	partie      *Partie
	initialized bool
	dx, dy      float64
}

func NewBackgroundAndPartie() *Background {
	return &Background{partie: NewPartie()}
}

func (b *Background) init() {
	if !b.initialized {
		imgBounds := BackgroundImg.Bounds()
		b.dx, b.dy = (ScreenWidth-float64(imgBounds.Dx()))/2, (ScreenHeight-float64(imgBounds.Dy()))/2
		b.partie = NewPartie()
		b.initialized = true
	}
}

func (b *Background) Update() error {
	b.init()
	if ebiten.IsKeyPressed(ebiten.KeyR) {
		b.partie = NewPartie()
	}
	return b.partie.Update()
}

func (b *Background) Draw(screen *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(b.dx, b.dy)
	screen.DrawImage(BackgroundImg, op)
	b.partie.Draw(screen)
}
