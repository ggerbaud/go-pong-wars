package main

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type PongGame struct {
	bg *Background
}

func NewPongGame() *PongGame {
	return &PongGame{NewBackgroundAndPartie()}
}

func (g *PongGame) Update() error {
	if ebiten.IsKeyPressed(ebiten.KeyEscape) {
		return ebiten.Termination
	}
	return g.bg.Update()
}

func (g *PongGame) Draw(screen *ebiten.Image) {
	g.bg.Draw(screen)
}

func (g *PongGame) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	ScreenWidth, ScreenHeight = float64(outsideWidth), float64(outsideHeight)
	return outsideWidth, outsideHeight
}

var (
	ScreenWidth  float64 = 1600
	ScreenHeight float64 = 1200
)

const (
	WallWidth    = 50
	WallHeight   = 5
	PlayerWidth  = 15
	PlayerHeight = 150
)
