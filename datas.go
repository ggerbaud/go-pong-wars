package main

type Point struct {
	X float64
	Y float64
}

type Collider struct {
	X      float64
	Y      float64
	Width  float64
	Height float64
}

func (c Collider) MaxX() float64 {
	return c.X + c.Width
}

func (c Collider) MaxY() float64 {
	return c.Y + c.Height
}

func (c Collider) Intersects(other Collider) bool {
	return c.X <= other.MaxX() &&
		other.X <= c.MaxX() &&
		c.Y <= other.MaxY() &&
		other.Y <= c.MaxY()
}

func (c Collider) Ratio(other Collider) float64 {
	half := c.Y + c.Height/2
	oHalf := other.Y + other.Height/2
	return 1.7 * (oHalf - half) / c.Height
}

const (
	speedCoeff = 1.05
)
