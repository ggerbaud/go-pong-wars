package main

import "github.com/hajimehoshi/ebiten/v2"

func main() {
	g := NewPongGame()

	//ebiten.SetWindowSize(1900, 1200)
	ebiten.SetFullscreen(true)
	ebiten.SetWindowTitle("Pong Wars")
	err := ebiten.RunGame(g)
	if err != nil {
		panic(err)
	}
}
