package main

import "github.com/hajimehoshi/ebiten/v2"

type Player interface {
	Update()
	Draw(screen *ebiten.Image)
	Collider() Collider
}
