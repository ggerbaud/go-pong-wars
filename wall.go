package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"image/color"
)

type Wall struct {
	width, height, x, y float64
	clr                 color.Color
}

func NewWall(width, height, x, y float64, clr color.Color) *Wall {
	return &Wall{
		width: width, height: height,
		x: x, y: y,
		clr: clr,
	}
}

func (w *Wall) Draw(screen *ebiten.Image) {
	img := ebiten.NewImage(int(w.width), int(w.height))
	img.Fill(w.clr)
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(w.x, w.y)
	screen.DrawImage(img, op)
}

func (w *Wall) Collider() Collider {
	return Collider{X: w.x, Y: w.y, Width: w.width, Height: w.height}
}
