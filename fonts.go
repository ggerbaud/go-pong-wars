package main

import (
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
)

var ScoreFont = mustLoadFont("assets/fonts/Kenney_Blocks.ttf", 48)
var CreditsFont = mustLoadFont("assets/fonts/Jungle_DF.ttf", 28)

func mustLoadFont(name string, size float64) font.Face {
	f, err := assets.ReadFile(name)
	if err != nil {
		panic(err)
	}

	tt, err := opentype.Parse(f)
	if err != nil {
		panic(err)
	}

	face, err := opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingVertical,
	})
	if err != nil {
		panic(err)
	}

	return face
}
